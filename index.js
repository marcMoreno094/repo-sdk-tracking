var express = require("express");
var bodyParser = require("body-parser");
var records = require("./records.js");
var gameapps = require("./gameapps.js");
var app = express();

var queries = {
	opcio: 'SELECT Value as Dia, count(*) as Usuaris FROM attribution where Key = "OPCIO" Group by Value',
	pop: 'SELECT Value as Dia, count(*) as Usuaris FROM attribution where Key = "POP" Group by Value',
	date: 'SELECT Value as Dia, count(*) as Usuaris FROM attribution where Key = "DATE" Group by Value'
};
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	extended: true
})); 

//app.route = '/records';
app.get('/analytics', function (req, res) {
	gameapps.getAll(req, res);
});
app.get('/analytics/:query', function (req, res) {
	records.get(req.params.query, [], req, res);
});

/*app.post('/records/:app/:ID/:puntuacio', function (req, res) {
	insert(req.params.app, req.params.ID, req.params.puntuacio, req, res);
});*/

app.post('/analytics', function (req, res) {
	//console.log(req.body);
	records.insert(req.body, [], req, res);
});

app.get('/test', function(req, res){
	res.status(200);
	res.json({'Hello!' : 'test'});
});

app.listen(process.env.PORT || 8000, function () {
	console.log('Listening...');
});